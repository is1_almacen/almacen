package repository;

import domain.Solicitud;

public interface SolicitudRepository extends BaseRepository< Solicitud, Long > {
	String FindProovedorByIdSolicitud( Long ids );
	
	String FindRegister_UserByIdSolicitud( Long ids );
	
}
