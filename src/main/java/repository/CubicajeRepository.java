package repository;

import java.util.Collection;

import domain.Cubicaje;

public interface CubicajeRepository extends BaseRepository< Cubicaje, Long > {
	String FindByIdCubicaje( Long idc );
	
	Collection<Cubicaje> findByCUbicajeId(Long cubicajeId);
	}