package repository.jpa;

import javax.persistence.TypedQuery;

import repository.UsuarioRepository;
import domain.Usuario;

public class JpaUsuarioRepository extends JpaBaseRepository<Usuario, Long> implements
UsuarioRepository {

	@Override
	public String FindByIdUsuario( Long Idu ){
		String querys = "SELECT a FROM Solicitud a WHERE a.Id = :Idu";
		TypedQuery< Usuario > consulta = entityManager.createQuery(querys, Usuario.class ); 
		return consulta.getParameter( "id_Usuario", String.class ).toString();
		
	}
	
	
}