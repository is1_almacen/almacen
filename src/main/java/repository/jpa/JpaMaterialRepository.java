package repository.jpa;
import org.springframework.stereotype.Repository;

import repository.MaterialRepository;
import domain.Material_almacen;


import javax.persistence.TypedQuery;

@Repository
public class JpaMaterialRepository extends JpaBaseRepository< Material_almacen , Long  > 
implements MaterialRepository{
	
	public String GetPorcentajeAcumuladoByNombre( String nombre ){
		String jpaQuery = " SELECT a FROM Material_almacen a WHERE a.name = :nombre";
		TypedQuery<Material_almacen > consulta = entityManager.createQuery( jpaQuery, Material_almacen.class );
		return consulta.getParameter("porcentaje_acumulado", String.class ).toString();
	}

	public String GetPrecioUnitarioByNombre( String nombre ){
		String jpaQuery = " SELECT a FROM Material_almacen a WHERE a.name = :nombre";
		TypedQuery< Material_almacen> consulta = entityManager.createQuery( jpaQuery, Material_almacen.class );
		return consulta.getParameter("precio_unitario", String.class ).toString();
	
	}
	
	public Material_almacen GetMaterialByNombre( String Nombre ){
		String jpaQuery = " SELECT a FROM Material_almacen a WHERE a.name = :nombre";
		TypedQuery< Material_almacen> consulta = entityManager.createQuery( jpaQuery, Material_almacen.class );
		return consulta.getSingleResult();
	}
}

