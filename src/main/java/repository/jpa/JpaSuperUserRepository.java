package repository.jpa;

import java.util.Collection;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import repository.SuperUserRepository;
import domain.Super_user;

@Repository
public class JpaSuperUserRepository extends JpaBaseRepository<Super_user, Long> 
implements		SuperUserRepository {

	
	@Override
	public Super_user findBySu(String super_user) {
		//SELECT a.id, a.number, a.date FROM tbl_account a WHERE a.number = :number
		String jpaQuery = "SELECT a FROM  a WHERE a.number = :number";
		TypedQuery<Super_user> query = entityManager.createQuery(jpaQuery, Super_user.class);
		query.setParameter("super_user", super_user);
		return getFirstResult(query);
	}

	@Override
	public Collection<Super_user> findBySuId(Long super_userId) {
		String jpaQuery = "SELECT a FROM Account a JOIN a.owners p WHERE p.id = :personId";
		TypedQuery<Super_user> query = entityManager.createQuery(jpaQuery, Super_user.class);
		query.setParameter("super_userId", super_userId);
		return query.getResultList();
	}
}
	