package repository.jpa;


import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import domain.Solicitud;
import repository.SolicitudRepository;

@Repository
public class JpaSolicitudRepository extends JpaBaseRepository< Solicitud , Long > 
implements SolicitudRepository{
	
	@Override
	public String FindProovedorByIdSolicitud( Long ids ){
		String querys = "SELECT a FROM Solicitud a WHERE a.id = :ids";
		TypedQuery< Solicitud > consulta = entityManager.createQuery(querys, Solicitud.class ); 
		return consulta.getParameter( "sourceProveedor", String.class ).toString();
		
	}
	
	@Override
	public String FindRegister_UserByIdSolicitud( Long ids ){
		String querys = "SELECT a FROM Solicitud a WHERE a.id = :ids";
		TypedQuery< Solicitud > consulta = entityManager.createQuery(querys, Solicitud.class );
		return consulta.getParameter("Register_usert", String.class ).toString();
	}
}
