package repository.jpa;

import java.util.Collection;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import repository.VentaRepository;
import domain.Venta;

@Repository
public class JpaVentaRepository extends JpaBaseRepository<Venta, Long> 
implements		VentaRepository {

	
	@Override
	public Venta findByMaterial(String material) {
		//SELECT a.id, a.number, a.date FROM tbl_account a WHERE a.number = :number
		String jpaQuery = "SELECT a FROM  a WHERE a.number = :number";
		TypedQuery<Venta> query = entityManager.createQuery(jpaQuery, Venta.class);
		query.setParameter("material", material);
		return getFirstResult(query);
	}

	@Override
	public Collection<Venta> findByMaterialId(Long materialId) {
		String jpaQuery = "SELECT a FROM Account a JOIN a.owners p WHERE p.id = :personId";
		TypedQuery<Venta> query = entityManager.createQuery(jpaQuery, Venta.class);
		query.setParameter("materialId", materialId);
		return query.getResultList();
	}
	
	
}
