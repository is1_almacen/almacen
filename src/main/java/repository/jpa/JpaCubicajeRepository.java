package repository.jpa;

import java.util.Collection;

import javax.persistence.TypedQuery;

import repository.CubicajeRepository;
import domain.Cubicaje;

public class JpaCubicajeRepository extends JpaBaseRepository<Cubicaje, Long> implements
CubicajeRepository {

	@Override
	public String FindByIdCubicaje( Long idc ){
		String querys = "SELECT a FROM Cubicaje a WHERE a.id = :idc";
		TypedQuery< Cubicaje > consulta = entityManager.createQuery(querys, Cubicaje.class ); 
		return consulta.getParameter( "id_cubicaje", String.class ).toString();
		
	}
	
	@Override
	public Collection<Cubicaje> findByCUbicajeId(Long cubicajeId) {
		String jpaQuery = "SELECT a FROM Cubicaje a JOIN a.owners p WHERE p.id = :cubicajeId";
		TypedQuery<Cubicaje> query = entityManager.createQuery(jpaQuery, Cubicaje.class);
		query.setParameter("personId", cubicajeId);
		return query.getResultList();
	}
}