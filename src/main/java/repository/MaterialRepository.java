package repository;

import domain.Material_almacen;

public interface MaterialRepository extends BaseRepository< Material_almacen , Long > {
		String GetPorcentajeAcumuladoByNombre( String nombre );
		
		String GetPrecioUnitarioByNombre( String nombre );
		
		Material_almacen GetMaterialByNombre( String Nombre );
}
