package repository;

import java.util.Collection;

import domain.Super_user;


public interface SuperUserRepository extends BaseRepository<Super_user, Long> {
	Super_user findBySu(String super_user); 
	
	Collection<Super_user> findBySuId(Long super_userId);

}
