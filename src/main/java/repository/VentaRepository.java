package repository;

import java.util.Collection;

import domain.Venta;

public interface VentaRepository extends BaseRepository<Venta, Long> {
	Venta findByMaterial(String material);

	Collection<Venta> findByMaterialId(Long materialId);
}
