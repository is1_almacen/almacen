package service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import repository.SuperUserRepository;
import domain.Super_user;

@Service
public class SuperUserService {

	@Autowired
	SuperUserRepository superuserRepository;

	@Transactional
	public void save(Super_user super_user) {
		if (super_user.getId() == null) {
			superuserRepository.persist(super_user);
		} else {
			superuserRepository.merge(super_user);
		}
	}

	public Super_user get(Long id) {
		return superuserRepository.find(id);
	}

	public Collection<Super_user> getAll() {
		return superuserRepository.findAll();
	}
}
