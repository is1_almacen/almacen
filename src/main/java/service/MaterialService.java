package service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import repository.MaterialRepository;
import domain.Material_almacen;
import java.util.Collection;


@Service
public class MaterialService {

	@Autowired
	MaterialRepository MaterialS;
	
	@Transactional
	public void Save( Material_almacen material_ ){
		MaterialS.persist(material_);
		return ;
	}
	
	@Transactional
	public Material_almacen FindByID( Long id_ ){
		return MaterialS.find(id_);
	}
	
	@Transactional
	public Collection< Material_almacen > FindAll(){
		return MaterialS.findAll();
	}
	
	@Transactional
	public  void Remove( Material_almacen material_ ){
		MaterialS.remove(material_);
		return;
	}
	
	
}
