package service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import domain.Solicitud;
import repository.SolicitudRepository;
import java.util.Collection;

@Service
public class SolicitudService {

	@Autowired
	SolicitudRepository SolicitudR;
	
	@Transactional
	public void SaveSolicitud( Solicitud solicitudS){
		SolicitudR.persist(solicitudS);
	}
	
	public Solicitud GetSolicitudById( Long ids ){
		return SolicitudR.find(ids);
	}
	//// obtiene el sourceregister de un solicitud por su ids
	public String GetSourceRegisterById( Long ids ){
		return SolicitudR.FindRegister_UserByIdSolicitud(ids);
	}
	/// obtiene el proovedor de un solicitud por su ids
	public String GetProveedorById( Long ids ){
		return SolicitudR.FindProovedorByIdSolicitud(ids);
	}
	
	///retorna todas la collectiones actuales en la DB
	public Collection< Solicitud > GetAllSolicitud(){
		return SolicitudR.findAll();
	}
	
	public void Remove( Solicitud solicitud_ ){
		SolicitudR.remove(solicitud_);
		return ;
	}
}
