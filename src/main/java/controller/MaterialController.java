package controller;

import service.MaterialService;
import domain.Material_almacen;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class MaterialController {

	@Autowired
	MaterialService Service;
	
	@RequestMapping( value = "/SaveMaterial" ,  method = RequestMethod.POST )
	String Save( @ModelAttribute Material_almacen material , ModelMap modelo ){
		Service.Save(material);
		return "material";
	}
	
	@RequestMapping( value = "/BorrarMaterial" ,  method = RequestMethod.POST )
	String Remove( @RequestParam Long id , ModelMap modelo ){
		Service.Remove( Service.FindByID(id) );
		modelo.addAttribute( "MaterialActual" , Service.FindAll()  );
		return "show-all-material";
	}
	
	@RequestMapping( value = "/Nuevo" , method = RequestMethod.GET )
	String AddMaterial( @RequestParam( required = false ) Long id , ModelMap modelo ){
		Material_almacen materiales = id == null ?  new Material_almacen() : Service.FindByID(id) ;
		modelo.addAttribute("Material_almacen", materiales);
		return "add-material";
	}
	
	@RequestMapping( value = "/materiales" , method = RequestMethod.GET )
	String ShowPage( @RequestParam(required = false) Long id, ModelMap modelo  ){
		if( id > 0 ){
			modelo.addAttribute("Material_almacen", Service.FindByID(id) );
			return "borrar-material";
		}
		if( id == -1 ){
			modelo.addAttribute( "MaterialActual" , Service.FindAll()  );
			return "show-all-material";
		}
		else{
			return "material";
		}
		
	}
	
	
	
	
}
