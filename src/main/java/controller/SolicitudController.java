package controller;


import service.SolicitudService;
import domain.Solicitud;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class SolicitudController {

	@Autowired
	SolicitudService ServiceS;
	
	@RequestMapping(  value = "/savesolicitud" , method = RequestMethod.POST )
	String SaveSolicitud( @ModelAttribute Solicitud solicitud , ModelMap modelo ){
		ServiceS.SaveSolicitud( solicitud );
		modelo.addAttribute("solicitudes" , ServiceS.GetAllSolicitud() );
		return "solicitud";
	}
	
	@RequestMapping( value = "add-solicitud" , method = RequestMethod.GET )
	String AddSolicitud( @RequestParam( required = false) Long ids , ModelMap modelo ){
		Solicitud solicitar = ids == null ? new Solicitud() : ServiceS.GetSolicitudById(ids);
		modelo.addAttribute( "Solicitud" , solicitar );
		return "add-solicitud";
	}
	
	/// redirige las capas de presentacion
	@RequestMapping( value = "solicitud" , method = RequestMethod.GET )
	String ShowSolicitud( @RequestParam( required = false ) Long ids , ModelMap modelo ){
		modelo.addAttribute("solicitudes" , ServiceS.GetAllSolicitud() );
		return "solicitud";
	}
	
}
