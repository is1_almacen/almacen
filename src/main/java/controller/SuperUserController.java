package controller;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import service.SuperUserService;
import domain.Super_user;

@Controller
public class SuperUserController {
	
	@Autowired
	SuperUserService superuserService;

	@RequestMapping(value = "/superuser", method = RequestMethod.POST)
	String saveSuper_user(@ModelAttribute Super_user super_user, ModelMap modela) {
		System.out.println("saving: " + super_user.getId());
		superuserService.save(super_user);
		return showUser(super_user.getId(), modela);
	}
	@RequestMapping(value = "/add-user", method = RequestMethod.GET)
	String addNewUser(@RequestParam(required = false) Long id, ModelMap modela) {
		Super_user super_user = id == null ? new Super_user() : superuserService.get(id);
		modela.addAttribute("super_user", super_user);
		return "add-user";
	}

	@RequestMapping(value = "/user", method = RequestMethod.GET)
	String showUser(@RequestParam(required = false) Long id, ModelMap modela) {
		if (id != null) {
			Super_user super_user = superuserService.get(id);
			modela.addAttribute("super_user", super_user);
			return "super_user";
		} else {
			Collection<Super_user> user = superuserService.getAll();
			modela.addAttribute("User", user);
			return "user";
		}
	}

}