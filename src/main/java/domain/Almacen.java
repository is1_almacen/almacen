package domain;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
public class Almacen implements BaseEntity<Long> {

	@Id
	@SequenceGenerator(name = "Almacen_id_generator", sequenceName = "Almacen_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Almacen_id_generator")
	private Long id;

	@Column(length = 64)
	private String capacidad;
	
	@Column(length = 64)
	private String Lista_cubicaje;
   
	@ManyToMany
	@JoinTable(name="Almacen")
		private Collection<Register_user> sourceRegister;
    
		
	
	@OneToMany(mappedBy = "sourceRegister_user", fetch = FetchType.LAZY)
	private Collection<Solicitud> one_many_en_register_user;

	
	@OneToMany(mappedBy = "sourceAlmacen", fetch = FetchType.LAZY)
	private Collection<Cubicaje> one_many_en_cubicaje;
	
	
	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}
	


}
