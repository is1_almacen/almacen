package domain;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
public class Ventas_user implements BaseEntity<Long> {

	@Id
	@SequenceGenerator(name = "Ventas_user_id_generator", sequenceName = "Ventas_user_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Ventas_user_id_generator")
	private Long id;

	@Column(length = 64)
	private String firstName;
	
   
	@OneToMany(mappedBy = "sourceVentas_user", fetch = FetchType.LAZY)
	private Collection<Venta> one_many_en_ventas;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}
	


}
