package domain;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
public class Register_user implements BaseEntity<Long> {

	@Id
	@SequenceGenerator(name = "Register_user_id_generator", sequenceName = "Register_user_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Register_user_id_generator")
	private Long id;

	@Column(length = 64)
	private String Name;
	
	
	@ManyToOne
	private Usuario sourceUsuario;
	
	@ManyToMany
	private Collection<Almacen> sourceRegister;
	
	@OneToMany(mappedBy = "sourceRegister_user", fetch = FetchType.LAZY)
	private Collection<Solicitud> one_many_en_register_user;

	
	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}
	


}
