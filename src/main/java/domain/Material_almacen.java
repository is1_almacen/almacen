package domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity
public class Material_almacen implements BaseEntity<Long> {

	@Id
	@SequenceGenerator(name = "Material_id_generator", sequenceName = "Material_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Material_id_generator")
	private Long id;

	@Column(length = 64)
	private String name;
	
	@Column(length = 64)
	private String capacidad;
	
	@Column(length = 64)
	private String precio_unitario;
	
	@Column(length = 64)
	private String nombre_proveedor;
	
	@Column(length = 64)
	private String fecha_ing;
	
	@Column(length = 64)
	private String fecha_egr;
	
	@Column(length = 64)
	private String porcentaje_part;
	
	@Column(length = 64)
	private String porcentaje_acumulado;
	
	//@OneToMany(mappedBy = "sourceMaterial_almacen", fetch = FetchType.LAZY)
	//private Collection<Almacen> one_many_en_almacen;
	
	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}
	


}
