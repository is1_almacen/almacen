package domain;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
public class Proveedor implements BaseEntity<Long> {

	@Id
	@SequenceGenerator(name = "Proveedor_id_generator", sequenceName = "Proveedor_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Proveedor_id_generator")
	private Long id;

	@Column(length = 64)
	private String Name;
	
	@OneToMany(mappedBy = "sourceProveedor", fetch = FetchType.LAZY)
	private Collection<Solicitud> one_many_en_proveedor;
	
	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}
	


}
