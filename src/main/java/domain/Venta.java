package domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import javax.persistence.SequenceGenerator;

@Entity
public class Venta implements BaseEntity<Long> {

	@Id
	@SequenceGenerator(name = "Venta_user_id_generator", sequenceName = "Venta_user_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Venta_user_id_generator")
	private Long id;

	@Column(length = 64)
	private String material ;

	@Column (length = 64)
	private String cantidad;
	
	@ManyToOne
	private Ventas_user sourceVentas_user;
	
	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}
	


}
