package domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity

public class Cubicaje implements BaseEntity<Long> {
	
	@Id
	@SequenceGenerator(name = "Cubicaje_id_generator", sequenceName = "Cubicaje_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Cubicaje_id_generator")
	private Long id;

	@Column(length = 64)
	private String pasillo;
	
	@Column(length = 64)
	private String nivel;
	
	@Column(length = 64)
	private String estanteria;
	
	@Column(length = 64)
	private String estado;
	
	@Column(length = 64)
	private Long id_cubicaje;
	
   		
	
//	@OneToMany(mappedBy = "sourceCubicaje", fetch = FetchType.LAZY)
//	private Collection<Solicitud> cubicajes;
	
	@ManyToOne
	private Almacen sourceAlmacen;
	
	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public Long getId_Cubicaje() {
		return id_cubicaje;
	}

	public void setId_cubicaje(Long id_cubicaje) {
		this.id_cubicaje = id_cubicaje;
	}
}
