package domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
public class Solicitud implements BaseEntity<Long> {

	@Id
	@SequenceGenerator(name = "Solicitud_id_generator", sequenceName = "Solicitud_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Solicitud_id_generator")
	private Long id;

	@Column(length = 64)
	private String material;

	@Column(length = 64)
	private String cantidad;
	
	@Column(length = 64)
	private String estado;



	@ManyToOne
	private Register_user sourceRegister_user;

	@ManyToOne
	private Proveedor sourceProveedor;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getMaterial() {
		return material;
	}

	public void setMaterial(String material) {
		this.material = material;
	}

	public String getcantidad() {
		return cantidad;
	}

	public void setcantidad(String cantidad) {
		this.cantidad = cantidad;
	}



}
