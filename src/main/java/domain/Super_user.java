package domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import javax.persistence.SequenceGenerator;

@Entity
public class Super_user implements BaseEntity<Long> {

	@Id
	@SequenceGenerator(name = "Super_user_id_generator", sequenceName = "Super_user_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Super_user_id_generator")
	private Long id;

	@Column(length = 64)
	private String firstName;
	
   
	@ManyToOne
	private Usuario sourceUsuario;
	
	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}
	


}
